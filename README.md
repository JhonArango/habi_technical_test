# Prueba técnica

El proyecto actual consta de dos microservicios principales: 
 - El primero de ellos es practico donde se mostrara codigo funcional de API REST donde los usuarios podran visualizar algunos inmuebles de acuerdo a requerimientos especificos.
 - El segundo es teorico donde se planteara únicamente el modelo que supla el requerimineto y crear el código SQL para las posibles consultas.
## 1) Microservicio API REST (Servicio de consulta)

Se plantea el siguiente arbol de directorios para el proyecto:

```bash
|-- habi_technical_test
    |-- README.md
    |-- .gitignore
    |-- .env
    |-- core
        |-- __init__.py
        |-- request.py
        |-- server.py
        |-- apis
            |-- __init__.py
            |-- test
                |-- __init__.py
                |-- test_functions.py
            |-- utils
                |-- __init__.py
                |-- functions.py
            |-- property_create.py
            |-- property_detail.py
            |-- property_list.py
        |-- config
            |-- __init__.py
            |-- connect_success.py
            |-- connection_db_habi.py
            |-- settings.py.py
        |-- urls
            |-- __init__.py
            |-- property.js
    |-- media_readme
        |-- Diagrama_Clases_MS_1.jpg
        |-- Diagrama_Clases_MS_2.jpg
        |-- Diagrama_ER_Ms_1.jpg
        |-- Diagrama_ER_Ms_2.jpg

```
### Pila tecnologica 

- Python
- Mysql
- Git
- Postman
- SQL
- virtualenv
- Lucidchart


### Variables de entorno

Para ejecutar este proyecto, se necesita añadir las sguientes variables de entorno en tu archivo .env

Por efectos de revision del proyecto se dejara el .env en el repositorio

#### _Variables de autenticación_

`KEY_BEARER`

#### _Variables de acceso a la DB_

`DB`

`USER_DB`

`HOST`

`PORT`

`PASSWORD`


### Ejecutar localmente

- Crear entorno virtual

```bash
  virtualenv habi_env
```

- Clonar proyecto

```bash
  https://gitlab.com/JhonArango/habi_technical_test.git core
```

- Ir al directorio del proyecto

```bash
  cd core
```

- Instalar dependencias

```bash
  pip install -r requirements.txt
```

Start the server

```bash
  python server.py
```


### Ejecutar tests

- Para ejecutar los test escribir los siguientes comandos:

```bash
  cd core
```

```bash
  python -m unittest -v
```


### Pasos para la solución de requerimientos

- Realizar coneccion a la db con credenciales enviadas
- Identificar modelos asignados en la db para las pruebas
- Crear diagrama de modelos para identificar los requerimientos
- Crear archivo .env para proteccion de variables
- Generar servidor base para escuchar sobre puerto 8000 y recibir peticiones apis
- Generar funcion para validacion de autenticacion basica del servidor
- Crear urls para recibir peticiones especificas por applicaciones (property inicialmente)
- Crear directorio de apis para distintos tipos de peticiones GET - POST
- Crear api para listar propiedades de acuerdo a filtros usados ( Solo con estado pre_venta - en_venta - vendido)
- Crear api para obtener los detalles de una propiedad
- Crear api para crear nueva propiedad y asignarle un status_history como en_venta
- Generar documentacion api en postman para realizar peticiones


### A tener en cuenta

- En los requerimientos se pide mostrar: Dirección, ciudad, estado, precio de venta y descripción de un inmueble,
  pero a la hora de hacer un filtro el usuario puede hacerlo por año de creacion, por esta razon por usabilidad 
  se agrega a los atributos que pueden ser visualizados.

- Algunos inmuebles no tenian precio, descripcion, etc. Por usabilidad se realiza consulta descardandolos. 

- Si se desea hacer alguna modificacion en el repositorio tener en cuenta que la rama master esta protegida, asi mismo
  rama development solo permite realizar merge.


### Diagramas

#### Diagrama de clases

![Alt text](/media_readme/Diagrama_Clases_MS_1.jpg?raw=true "Diagrama Clases 1")

#### Diagrama entidad relacion

![Alt text](/media_readme/Diagrama_ER_Ms_1.jpg?raw=true "Diagrama ER 1")


### Documentacion Api Postman

[Link postman - Referencia api](https://documenter.getpostman.com/view/5321463/UVC8DS16)

## 2) Microservicio Documentado (Servicio de “Me gusta”)

### Diagrama de clases

![Alt text](/media_readme/Diagrama_Clases_MS_2.jpg?raw=true "Diagrama Clases 2")

### Diagrama entidad relacion

![Alt text](/media_readme/Diagrama_ER_Ms_2.jpg?raw=true "Diagrama ER 2")

### SQL

#### Creacion de tablas

Crear user

```bash
    CREATE TABLE user (
        id INT AUTO_INCREMENT,
        first_name VARCHAR(64) NOT NULL,
        last_name VARCHAR(64) NOT NULL,
        dni INT(64),
        gender VARCHAR(32),
        email VARCHAR(120) NOT NULL,
        cellphone VARCHAR(32),
        PRIMARY KEY (id)
    );
```

Crear like_history

```bash
    CREATE TABLE like_history (
        id INT AUTO_INCREMENT,
        id_user INT(32) NOT NULL REFERENCES user (id) ON DELETE CASCADE,
        id_property INT(32) NOT NULL REFERENCES property (id) ON DELETE CASCADE,
        type VARCHAR(64) NOT NULL,
        create_date TIMESTAMP,
        PRIMARY KEY (id)
    );
```
#### SQL para apis

Ejecutar like

```bash
    INSERT INTO like_history (id_user, id_property, type, create_date)
        VALUES ('1','1','like',STR_TO_DATE("11-13-2021 20:40:10", "%m-%d-%Y %H:%i:%s"));
```
Consultar inmuebles con like de un usuario especifico  

```bash
    SELECT P.address, P.city, P.description, P.price, P.year  FROM property P
    JOIN like_history LH
    ON P.id = LH.id_property
    WHERE LH.id_user = '{id_user}' AND LH.type = 'like';
```

Consultar inmuebles con mas likes ( Count_of_likes = cantidad de likes por inmueble)

```bash
    SELECT DISTINCT LH.id_property, count(LH.id_property) as Count_of_likes, P.description, P.price, P.address, P.city, P.year FROM like_history LH
    JOIN property P
    ON LH.id_property = P.id
    WHERE type = 'like'
    GROUP BY id_property
    ORDER BY Count_of_likes DESC;
```

#### Explicacion MS "Me gusta".
Un usuario registrado en la plataforma puede dar likes a los inmuebles, por esto se debe crear una relacion entre la entidad property y user.

Por ende sabemos que un usuario puede dar like a varios inmuebles y asi mismo un inmueble puede contener muchos likes de distintos usuarios, esto se convierte en una relacion muchos a muchos; entendiendo por escalabilidad de la funcionalidad (like-dislike) se crea una tabla intermedia por el tipo de accion y el tiempo (timestamp) en que se ejecuta la misma.

### Feedback

Si tiene algúna retroalimentacion, por favor escribir a jhonarang.93@utp.edu.co
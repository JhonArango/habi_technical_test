import json
from apis import api_list_property, api_detail_property, api_create_property

RESPONSE_DEFAULT = {
    "status_code": 400,
    "response": {
        "success": False,
        "error": "Invalid path"
    }
}

def get_url_properties(complement_path, params):
    response = RESPONSE_DEFAULT
    if complement_path.startswith('list/'):
        response = api_list_property(params)
    if complement_path.startswith('detail/'):
        if "id_property" in params:
            response = api_detail_property(params["id_property"])
        else:
            response["response"]["error"] = "Missing parameter"
    return response

def post_url_properties(complement_path, body):
    response = RESPONSE_DEFAULT
    if complement_path.startswith('new/'):
        response = api_create_property(body)
    return response
        
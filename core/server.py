import json
from urllib.parse import urlparse
from config import AUTHORIZATION_KEY
from urls import get_url_properties, post_url_properties
from http.server import BaseHTTPRequestHandler, HTTPServer


class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    # Validacion de token de acceso
    def do_VALIDATE_AUTH(self):
        header_auth = self.headers.get('Authorization')
        if header_auth == None:
            response = {
                'success': False,
                'error': 'No auth header received'
            }
        elif header_auth != 'Bearer ' + str(AUTHORIZATION_KEY):
            response = {
                'success': False,
                'error': 'Invalid credentials'
            }
        else:
            response = {
                'success': True,
                'error': 'valid credentials'
            }
        return response

    def do_GET(self):
        request_path = self.path
        validate_credencials = self.do_VALIDATE_AUTH()
        if not validate_credencials["success"]:
            self.send_response(401)
            self.do_HEAD()
            self.wfile.write(json.dumps(
                validate_credencials).encode('utf-8'))
            return
        else:
            # Se obtiene los parametros de la url para
            # enviarlos de manera limpia a las apis.
            query_params = urlparse(request_path).query
            query_params_dict = {}
            if len(query_params) > 0:
                query_params_dict = dict(qc.split("=")
                                         for qc in query_params.split("&"))

            if request_path.startswith('/property'):
                complement_path = request_path.lstrip("/property/")
                response = get_url_properties(
                    complement_path, query_params_dict)
                self.send_response(response["status_code"])
                self.do_HEAD()
                self.wfile.write(json.dumps(
                    response["response"]).encode('utf-8'))
            else:
                self.send_response(400)
                self.do_HEAD()
                self.wfile.write(json.dumps(
                    {'success': False,
                        'error': 'Invalid path'}).encode('utf-8'))

    def do_POST(self):
        request_path = self.path
        validate_credencials = self.do_VALIDATE_AUTH()
        if not validate_credencials["success"]:
            self.send_response(401)
            self.do_HEAD()
            self.wfile.write(json.dumps(
                validate_credencials).encode('utf-8'))
            return
        else:
            # Se obtienen los parametros del body de la peticion POST,
            # para enviarlos de manera limpia a las apis
            length = int(self.headers.get('Content-Length'))
            if length > 0:
                body = json.loads(self.rfile.read(length))
            if request_path.startswith('/property'):
                complement_path = request_path.lstrip("/property/")
                response = post_url_properties(
                    complement_path, body)
                self.send_response(response["status_code"])
                self.do_HEAD()
                self.wfile.write(json.dumps(
                    response["response"]).encode('utf-8'))
            else:
                self.send_response(400)
                self.do_HEAD()
                self.wfile.write(json.dumps(
                    {'success': False,
                        'error': 'Invalid path'}).encode('utf-8'))

    do_PUT = do_POST
    do_DELETE = do_GET


def main():
    port = 8000
    print(f"Listening Server Habi on localhost:{port}")
    server = HTTPServer(('', port), Server)
    server.serve_forever()


if __name__ == "__main__":
    main()

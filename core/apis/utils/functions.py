def create_where_filter_params(query_params):
    filter_by_query = ""
    if "year" in query_params:
        query_params_prep = "','".join(query_params["year"].split(","))
        filter_by_query += f" AND P.year in ('{query_params_prep}')"
    if "city" in query_params:
        query_params_prep = "','".join(query_params["city"].split(","))
        filter_by_query += f" AND P.city in ('{query_params_prep}')"
    if "state" in query_params:
        query_params_prep = "','".join(query_params["state"].split(","))
        filter_by_query += f" AND S.name in ('{query_params_prep}')"
    else:
        filter_by_query += f" AND S.name in ('vendido', 'en_venta', 'pre_venta')"
    return filter_by_query


def generate_object_response(object_bd):
    response = {
        "id_property": object_bd["id"],
        "city": object_bd["city"],
        "year": object_bd["year"],
        "price": object_bd["price"],
        "address": object_bd["address"],
        "label_state": object_bd["label"],
        "description": object_bd["description"],
    }
    return response


def validator_params_create_property(params):
    keys = ["address", "city", "price", "description", "year"]
    for key in keys:
        if not key in params:
            return False
        else:
            if not params[key]:
                return False
    return True

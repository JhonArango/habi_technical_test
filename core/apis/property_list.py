from config import database_connect
from apis.utils import create_where_filter_params, generate_object_response


def api(query_params):
    filter_by_query = ""
    if query_params:
        filter_by_query = create_where_filter_params(query_params)
    else:
        filter_by_query += (
            f" AND S.name in ('vendido', 'en_venta', 'pre_venta')"
        )
    properties_query = f"""
      SELECT P.id, P.address, P.city, S.label, P.price, P.description, P.year
      FROM property P
      JOIN status_history SH
      ON P.id = SH.property_id
      JOIN status S
      ON S.id = SH.status_id
      WHERE P.description != 'None' AND P.price != 'None'
      {filter_by_query} 
      AND Exists(
         SELECT 1
         FROM   status_history
         WHERE  property_id = SH.property_id
         HAVING SH.update_date = Max(update_date)
      )
      ORDER BY P.year
    """
    try:
        database_connect.cursor.execute(properties_query)
        records = database_connect.cursor.fetchall()
    except:
        records = []
    response = [generate_object_response(record) for record in records]
    return {"response": response, "status_code": 200}

from datetime import datetime
from config import database_connect
from .utils import validator_params_create_property
RESPONSE_DEFAULT = {
    "response": {"success": False, "error": "No valid data"},
    "status_code": 400,
}

def api(body):
    response = RESPONSE_DEFAULT
    if not validator_params_create_property(body):
        return response
    else:
        try:
            properties_query_create = f"""
                INSERT INTO property (address, city, price, description, year)
                VALUES ('{body["address"]}', '{body["city"]}', '{body["price"]}', '{body["description"]}', {body["year"]})
            """
            database_connect.cursor.execute(properties_query_create)

            status_history_query_create = f"""
                INSERT INTO status_history (property_id, status_id, update_date)
                VALUES ('{database_connect.cursor.lastrowid}', '3', '{datetime.now()}')
            """
            database_connect.cursor.execute(status_history_query_create)

            database_connect.connection.commit()
            response = {"response": {"message": "property created"}, "status_code": 201}
        except:
            print('Error query db')
    return response

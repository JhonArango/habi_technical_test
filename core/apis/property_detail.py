from config import database_connect
from apis.utils import generate_object_response


def api(id_property):
    status_code = 200
    property_query = f"""
        SELECT P.id, P.address, P.city, S.label, P.price, P.description, P.year
        FROM property P
        JOIN status_history SH
        ON P.id = SH.property_id
        JOIN status S
        ON S.id = SH.status_id
        WHERE P.id={id_property}fds
    """
    try:
        database_connect.cursor.execute(property_query)
        data_db = database_connect.cursor.fetchone()
    except:
        data_db = None
    if data_db:
        response = generate_object_response(data_db)
    else:
        status_code = 400
        response = {"success": False, "error": "Object not found"}

    return {"response": response, "status_code": status_code}

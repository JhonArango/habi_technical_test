import unittest
from apis.utils import (
    create_where_filter_params,
    validator_params_create_property,
)


class TestCreateWhereFilterParams(unittest.TestCase):
    def test_create_where_filter_params_complete(self):
        query_params = {
            "year": "2021",
            "city": "pereira,bogota",
            "state": "pre_venta",
        }
        expected_response = " AND P.year in ('2021') AND P.city in ('pereira','bogota') AND S.name in ('pre_venta')"
        self.assertEqual(
            expected_response, create_where_filter_params(query_params)
        )

    def test_create_where_filter_params_only_one_param(self):
        query_params = {"city": "pereira,bogota"}
        expected_response = " AND P.city in ('pereira','bogota') AND S.name in ('vendido', 'en_venta', 'pre_venta')"
        self.assertEqual(
            expected_response, create_where_filter_params(query_params)
        )

    def test_create_where_filter_params_empty(self):
        query_params = {}
        expected_response = (
            " AND S.name in ('vendido', 'en_venta', 'pre_venta')"
        )
        self.assertEqual(
            expected_response, create_where_filter_params(query_params)
        )


class TestGenerateObjectResponse(unittest.TestCase):
    def test_validator_params_create_property_complete(self):
        object_bd = {
            "city": "pereira",
            "year": "2012",
            "price": "250000000",
            "address": "M1 C5 Panorama",
            "description": "Comoda casa al frente de centro comercial",
        }
        self.assertTrue(validator_params_create_property(object_bd))

    def test_validator_params_create_property_empty_param(self):
        object_bd = {
            "city": "pereira",
            "year": "2012",
            "price": "250000000",
            "address": "M1 C5 Panorama",
            "description": "",
        }
        self.assertFalse(validator_params_create_property(object_bd))

    def test_validator_params_create_property_not_params(self):
        object_bd = {
            "city": "pereira",
            "year": "2012",
        }
        self.assertFalse(validator_params_create_property(object_bd))


if __name__ == "__main__":
    unittest.main()

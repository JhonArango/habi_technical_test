import pymysql
from config import DB_PARAMS

class Database:
    def __init__(self):
        try:
            self.connection = pymysql.connect(
                host=DB_PARAMS["host"],
                user=DB_PARAMS["user"],
                password=DB_PARAMS["password"],
                db=DB_PARAMS["db"],
                port=int(DB_PARAMS["port"]),
                cursorclass=pymysql.cursors.DictCursor
            )
            self.cursor = self.connection.cursor()
        except:
            print("connection error")


    def show_tables(self):
        tablas = "SHOW TABLES;"
        self.cursor.execute(tablas)
        records = self.cursor.fetchall()
        print("Tablas habi DB")
        for record in records:
            print(record)

    def show_columns_table(self, title=None):
        if not title:
            print("Error en el ingreso del nombre de la tabla")
        else:
            records = f"SHOW COLUMNS FROM {title};"
            self.cursor.execute(records)
            records = self.cursor.fetchall()
            print(f"Columnas de {title}:")
            for record in records:
                print(record)

    def connection_close(self):
        self.connection.close()
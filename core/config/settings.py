import os
from dotenv import load_dotenv
load_dotenv()

#Key authorization bearer
AUTHORIZATION_KEY = os.getenv('KEY_BEARER')

#Params DB Habi
DB = os.getenv('DB')
HOST = os.getenv('HOST')
PORT = os.getenv('PORT')
USER_DB = os.getenv('USER_DB')
PASSWORD = os.getenv('PASSWORD')

DB_PARAMS = {
    "db":DB, 
    "host":HOST, 
    "port":PORT, 
    "user":USER_DB, 
    "password":PASSWORD, 
}